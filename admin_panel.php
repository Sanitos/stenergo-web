<?php session_start();
	include_once ('db_connecnt.php');
	if($_SESSION['is_auth']==false){
		header("Location:admin.php");
	}
	switch ($_GET['section']) {
		case 'add_clients':
			$admin_title="Клиенты";
			break;
		case 'add_news':
			$admin_title="Новости";
			break;
		case 'add_object':
			$admin_title="Выполненные объекты";
			break;
		default:
			$admin_title="Управление контентом";
			break;
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Admin Panel</title>
</head>
<body>
	<?php 
	if($_SESSION['is_auth']==true){
		include_once("header.php");
		?>
		<div class="wrap-content wrap-column">
				<div class="admin-title">
					<h2><a href="admin_panel.php"><?php echo $admin_title ?></a></h2>
					<form action="admin.php" method="post">
						<input type="submit" name="logout_btn" value="Выйти">
					</form>
				</div>
				<?php 
		switch ($_GET['section']) {
			case 'add_clients':?><ul class="items-on-site">
				<?php
				$query="SELECT * FROM clients";
				$stmn=$pdo->prepare($query);
				$stmn->execute();
				while($row=$stmn->fetch()){?>
					<li><?php echo $row['name'] ?><div class="action-btn">
					<button class="client_edt" id="edt<?php echo $row['clients_id'] ?>">Редактировать</button>
					<button class="client_del" id="del<?php echo $row['clients_id'] ?>">Удалить</button></div></li>
				<?php } ?>
			</ul>
				<form class="add-new-form" action="php-script/admin_func.php" method ="post" enctype="multipart/form-data">
					<input type="hidden" name="id">
					<input type="text" name="name" placeholder="Имя компании">
					<select name="industry" placeholder="Отрасль" onchange="this.nextElementSibling.value=this.value">
						<option value="Нефтегазовый комплекс">Нефтегазовый комплекс</option>
						<option value="Другие секторы экономики">Другие секторы экономики</option>
						<option value="Промышленность">Промышленность</option>
						<option value="Аграрный комплекс">Аграрный комплекс</option>
					</select>	
				<div class="select-img">
						<input type="text" name="image_url" placeholder="URL изображения"> или <input type="file" name='userfile' value="">
					</div>
					<input type="text" name="url" placeholder="URL компании">
					<input type="submit" name="add_clients_btn">
				</form>
			<?php 
				break;
			case 'add_news':?>
			<ul class="items-on-site">
			<?php 
				$query="SELECT * FROM news";
				$stmn=$pdo->prepare($query);
				$stmn->execute();
				while($row=$stmn->fetch()){?>
					<li><?php echo $row['title'] ?>
						<div class="action-btn">
							<button class="news_edt" id="edt<?php echo $row['news_id'] ?>">Редактировать</button>
							<button class="news_del" id="del<?php echo $row['news_id'] ?>">Удалить</button>
						</div>
					</li>
				<?php } ?>
			</ul>
				<form class="add-new-form" action="php-script/admin_func.php" method ="post" enctype="multipart/form-data">
					<input type="hidden" name="id">
					<input type="text" name="title" placeholder="Заголовок">
					<input type="date" name="date" placeholder="Дата">
					<textarea rows="10" type="text" name="text" placeholder="Текст новости"></textarea>
					<div class="select-img">
						<input type="text" name="image_url" placeholder="URL изображения"> или <input type="file" name='userfile' value="">
					</div>
					<input type="submit" name="add_news_btn">
				</form>
			<?php 
				break;
			case 'add_object':?><ul class="items-on-site">
				<?php 
				$query="SELECT * FROM objects";
				$stmn=$pdo->prepare($query);
				$stmn->execute();
				while($row=$stmn->fetch()){?>
					<li><?php echo $row['title'] ?><div class="action-btn">
					<button class="objects_edt" id="edt<?php echo $row['objects_id'] ?>">Редактировать</button>
					<button class="objects_del" id="del<?php echo $row['objects_id'] ?>">Удалить</button></div></li>
				<?php } ?>
			</ul>
				<form class="add-new-form" action="php-script/admin_func.php" method="post" enctype="multipart/form-data">
					<input type="hidden" name="id">
					<input type="text" name="title" required placeholder="Название объекта">
					<input type="text" name="customer" required placeholder="Заказчик">
					<input type="text" name="object" required placeholder="Объект">
					<input type="text" name="task" required placeholder="Задача">
					<textarea rows="10" type="text" name="performance_work" required placeholder="Выполненные работы"></textarea>
					<input type="text" name="important_date" required placeholder="Важные даты">
					<div class="select-img">
						<input type="text" name="image_url" placeholder="URL изображения"> или <input type="file" name='userfile' value="">
					</div>
					<input type="submit" name="add_object_btn" required>
				</form>
			<?php
				break;
			default:?>
			
				<div class="action">
					<a class='action-item' href="admin_panel.php?section=add_clients">Клиенты</a> <br>
					<a class='action-item' href="admin_panel.php?section=add_news">Новости</a> <br>
					<a class='action-item' href="admin_panel.php?section=add_object">Выполненые объекты</a>
					
				</div>
				<?php break;
		}
		?></div><?php
		include_once ("footer.php");

		?>
		
	<?php }?>
	
</body>
</html>
