<?php 
$page_title = "Наши клиенты";
include_once("header.php");
 ?>
	<?php
		$query='SELECT * FROM clients ';
		$stmn=$pdo->prepare($query);
		$stmn->execute();
		$industry = array();
		while($row=$stmn->fetch()){
			array_push($industry, $row['industry']);
		}
		$unique = array_unique($industry);
		foreach ($unique as $field) {?>
			<h2><?php echo $field; ?></h2>
			<div class="wrap-content flex-wrap">
			<?php 
			$query='SELECT * FROM clients WHERE industry = ? ';
			$stmn=$pdo->prepare($query);
			$stmn->execute([$field]);
			while($row=$stmn->fetch()){?>
				<div class="client-wrap">
				<a class='client' href="<?php echo $row['url'] ?>">
					<img src="<?php 
						if($row['image_url']!==''){
							echo $row['image_url']; 
						}else{
							echo 'image/default-client.png';
						}
					?>">
					<span class="client-title"><?php echo $row['name'] ?></span>
				</a>
				</div>
				<?php
				
			}?></div><?php 

		}
		?>
		

 <?php include_once('footer.php') ?>