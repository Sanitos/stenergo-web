<?php 
	$page_title = "Контакты";
	include_once ("header.php");

?>

<div class="wrap-content">
	<div class="text">
		<h1>Контактная информация</h1>
		<div class="contact">
			<div class="contact-card">
				<h2>ООО «СтандартЭнерго»</h2>
				<span><strong>Адрес: </strong>38164, Украина, Полтавская область, <br>Зеньковский район пгт. Опошня, ул. Заливчего, 71</span><br>
				<span><strong>Телефон: </strong><a href="tel:0535342987">(05353) 42-987</a></span><br>
				<span><strong>Факс: </strong>(05353) 42-956, (05353) 42-467</span><br>
				<span><strong>E-mail: </strong><a href="mailto:office@standartenergo.com">office@standartenergo.com</a></span><br>
			</div>	
			<div class="contact-map" id="map">
				<div class="mapouter"><div class="gmap_canvas"><iframe width="100%" height="400" id="gmap_canvas" src="https://maps.google.com/maps?q=%D0%A1%D1%82%D0%B0%D0%BD%D0%B4%D0%B0%D1%80%D1%82%D0%95%D0%BD%D0%B5%D1%80%D0%B3%D0%BE&t=k&z=15&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://www.pureblack.de/wordpress-agentur/"></a></div><style>.mapouter{text-align:right;height:400px;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:400px;width:100%;}</style></div>
			</div>
		</div>
		<form class='feedback' method="post" action="send_mail.php">
			<h2>Обратная связь</h2>
			<input type="text" name='name' required placeholder="Имя">
			<input type="email" name='email' required placeholder="E-Mail">
			<input type="tel" name='phone' placeholder="Телефон">
			<textarea rows="10" resi name='msg' required multiple="10" placeholder="Сообщение"></textarea>
			<input class='feedback_btn' type="submit" name="send_mail_btn" >
		</form>
	</div>
</div>
<?php 
	include_once ("footer.php");
?>
