-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Янв 31 2019 г., 12:56
-- Версия сервера: 5.6.38
-- Версия PHP: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `stenergo`
--
CREATE DATABASE IF NOT EXISTS `stenergo` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `stenergo`;

-- --------------------------------------------------------

--
-- Структура таблицы `admin`
--

CREATE TABLE `admin` (
  `user_id` int(11) NOT NULL,
  `login` varchar(40) NOT NULL,
  `salt` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `admin`
--

INSERT INTO `admin` (`user_id`, `login`, `salt`, `password`) VALUES
(1, 'admin', '2Kc-nWLP', '112a69aef66d6bc2be163525aea360e1');

-- --------------------------------------------------------

--
-- Структура таблицы `clients`
--

CREATE TABLE `clients` (
  `clients_id` int(11) NOT NULL,
  `name` text NOT NULL,
  `industry` text NOT NULL,
  `image_url` text NOT NULL,
  `url` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `clients`
--

INSERT INTO `clients` (`clients_id`, `name`, `industry`, `image_url`, `url`) VALUES
(54, 'Укр Нафта', 'Нефтегазовый комплекс', 'https://www.ukrnafta.com/images/logo.png', 'https://www.ukrnafta.com/'),
(55, 'Гадяцьке АТП', 'Промышленность', '', 'https://gadyacke-atp-15337.business-guide.com.ua/'),
(56, 'Райз-цукор', 'Аграрный комплекс', 'https://i.biz-gid.com/img/logo/42392.png', 'https://rise-sugar.business-guide.com.ua/'),
(57, 'Приватбанк', 'Другие секторы экономики', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQ4AAACqCAMAAABrlVHbAAAAdVBMVEX///9grSwBAABdrCf///3c7c9YqSDh79haqiNVqBj7/frq9ONmrzSNw2hosTnt9eiBvVp7u1K73KZztkb2+vNdXF3Q0NDi4uIPDg5iYWGIwWOy2Jp9u1Oq1JLP58GXyXbN5rzY7MzB362j0Ieby3yamppqampkqxmqAAAIYUlEQVR4nO2da2OrKBCGYTEIiRovnL2oibfs/v+fuDOoiUZt2rOb9jTM+6FNDII8GWZAgTBGIpFIJBKJRCKRSCQSiUQikUgkEulL5H3DnJ+nZ17zN+Rhds+T+erKfVyHQPDnSAj/qyv3cR0CJZ6lb4mDrGMiwjET4ZiJcMxEOGYiHDMRjpkIx0yEYybCMRPhmIlwzEQ4ZiIcMxGOmVZwCC21/LCW2bwGDhGFP6Nokc9r4FDxT2UUSsIxEeGYiXDMRDhmIhwzvSyO5PgzGb0qDs6zMjy1/gcfKb4sDiGk1JpnYfGRp6wvi+MKRYvsUiOR9zyKf3EctkZKy6zbvSsjB3D0RETcmscW4gYOrJfUSfrQiziDA6R00j4wD5dwcC5lDj7E22biFg7OtTiZN4C4hoMLXe62XapzOKDF8HTTPhzEAUGm229k5CIOaDDhRsh1Egd41Nys+g9HcXAdr9qHqzisfSwNxFkcXId7bxFfXgSHWDxxUw8B6e5VrcOcJ6rb9HTJswDG9W8jke0io9fAsSJzaIowE1pt4xB8UdVXxOF5o1MwTZepTRsRsrwPL6+IYyZzDmHctgFEn+7cx8vjAPmV2LCQRXNxAQdjTb5hIDp3zzpA+4LfV7TXXXRxBIfHmkyv4kjM1D7cwIGB5hCv8tDpNKEbOKxMvOZAVDINtg7hAB5r/kPWkySvgeOPH4/1N2PHbIWHnAaX18Dx+2+P9QP9KV82l1mF3cHxF8aXYsWdwsj2KqdwrLsPld2cqWM4WLMywpXna0au4WDVsrlMWotzOHyx8KaTrodrODyWL73HrcqO4YDeer3EceuoO4YDZLJFa9HVmJF7OFi3cKa3UOsgjvMi1t5uijmIY6W1XIdx7uHwlnXmuhg+dA6HtzZwkdXwvNY9HOA8FtYhx/UNzuEAHofFMF9kxj6r8hZ9tJfHgb50OY5LyjJJkjK6P+4ADrY2yldSvcw62g/iWBnVbskFHMt+qdM41m4ROowjJRxTHO3681pXcdRvTAkiHISDcBAOwkE4GOG4E+GYiXDMRDhmet90l6tq+e5d0pX6jjj+fKx/bsnPPHqvguAb4jB/PNZttxfPHI7v1uEb/sYC6b/ojUX4JBKJRPqf9Ku62nkMeFJE8O6XF/+qND5Ha7X/FYm0SXyn/Kd2EnykppyXknSPz/kCFYul5OLwjHLqu3J0/oxS/rPsw0MxLqrH18FTcPST574FDhHlveIpjqvvu7Vxb/7ndvju2Io/xulAIshvOtl0s3O8WymTl+wu4VOFOGQ4vME5O4hjj8IfCqzbs4Xj2SN7vChveLW/yrteOZ7Qnn27TdZ+Jg9xzNfEsWE5YVO39dn3bJ2HgsfT2XgAPsUXZms/oefiSLMkCU2aCa1lEOLOigUcSextCZPDqwvzk0FlEof9znp+FXGltRJZ5bM6KZOJqloIwHFnaMxr40BIDa3U7s6HmWapxVEleAm2MOt0Cygpuzz9TsAqjpNWMsq1xroJzVucrqGUbrASJpBKx8wHT6MkJrGOwHis5pgG3gqlA7/V9jM1JMpagdPD/APqaPfyBNPYV1oiDfs3N6yBTPXJckq00pkxAfxDL1NgKfFTYt47cODjdx10bVGijfMdAIL/O4sjElzmzMfqRWWcwFvBdcsOgYQDXZuGAqcJgnWUZcJtIngB1oGuNOiV5cWhXyeGJlO0Bc7K1SlrhN3CAHHEEudtD4WxFOxnc6upT8GhImwkppK4in4dh+6gjR9tigIyAtWYSy7xbjD2Q/0A0lYe7njTz6wc745KHUCb2GdgGHauuo9I8wkONsPRKvi4esfmn8/DIftZwsdIwSGzgQMSeJ3mQp/ZuSiK1F7uBdNaP3OwOGw+NtAK3TchjltXpsykcI5dFXbEhPGAwzrNCY6wBb+jL093o2/iGO/24zRQuStWG0tyqaoETxn26Nj7TZ1ecM+k/mx/jkNEBapLLJqo9wTGP7dFrviIg0cZKuAjDhEFYFDd50TaLRxD98Oz37U8DzjYDAe3XlKIyC7dORZ5xu0BPlrHHQ6V9MWYi/VNNWO7roysyWBmfWPB4NRnMuDgCpGsb8n2eTjGBReVFKLHIe5xiAgDIy58y312jjDGZHGVVnLDOvp+B0QUu1gOWmMnIISJJO/aDBuFxSEyO6a5WQeP0Nvkn3O3fdt31PaQB5YNjtH6jgYP2GZ+9R3QymuwZZ0fI7Qo3OsY51AOMDeso18cptsaA0uOvTYDHmrAsXSlYQpZ68/hsY2jP1hbq/UQR7+cDefATV0pY/DNiixVfKg5rmUZmtq978jODag+2V+skT5OxZWWcdoXuBVocZIqxtnnt5dtV6q6I3z1eOUQRltte1F7c47UFYcMz3Vt+xmAw/7d7c0ul1s4hkCrrHfRuYc4oBTPtIF6E4dN+Sk8trthUkeldQzY57QbL6ggSSS6ldGVQgdCYdgUusKADB22JLHbiI04+ASHuPU7cM9s6GKmwEUoKEVq8SYOGBkgj+r5sRbvd1wH2z7+dJUYcFSZ7YFrVeG3Al1wif3trAWXqUvm28+GnnhQHa8JdHzCgykOz3z8MazKfqfz+x1BjEMUUw2ZiC6EhNGxwTNtZwY66XDAHAPdX90xgaSf0C/1U9C4QN7gG7hQ6zvOxyIswevvWB8Nujgp8/TIWkhSe8f0KjvqhQSnPCnjqjV7e9CM+fWZH27p09oOk5FXXcVlmXcN84s0LXY208YO8GvMY29sYXaBDH5UfM2z7Wtk8W6D97u7E6vn9SnG1Oyt1OO95Okp7Fba/f2Oj9bgf5Q3DbQkwAFdTcIxCHFwwjEK/Faz2zXmKxvsLySamkEikUgkEolEIpFIJBKJRCKRvrv+BYfuv8zid+m8AAAAAElFTkSuQmCC', 'https://privatbank.ua');

-- --------------------------------------------------------

--
-- Структура таблицы `news`
--

CREATE TABLE `news` (
  `news_id` int(11) NOT NULL,
  `title` text NOT NULL,
  `date` date NOT NULL,
  `text` text NOT NULL,
  `image_url` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `news`
--

INSERT INTO `news` (`news_id`, `title`, `date`, `text`, `image_url`) VALUES
(37, 'Новость 1', '2019-01-24', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos omnis molestias tenetur, tempora vero dignissimos explicabo quia placeat et doloremque quasi sequi adipisci earum illum veritatis commodi suscipit delectus sint repudiandae fugiat ad facilis mollitia. Minima nam eos nihil dolore eaque dicta explicabo, quidem perferendis quisquam provident consequuntur, placeat temporibus. Delectus reprehenderit cupiditate iste adipisci, sunt non, fugiat, culpa nam quis iusto ratione ipsum odio ab. Consequatur eum unde dolor id libero, minima ab aperiam dolores sit. Est quam, deleniti maiores veniam optio autem cum explicabo, atque consectetur minima dicta quis sed magni nobis, dolores. Possimus quidem maxime sequi exercitationem!', 'https://picsum.photos/200?random'),
(38, 'Новость 2', '2019-01-25', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos omnis molestias tenetur, tempora vero dignissimos explicabo quia placeat et doloremque quasi sequi adipisci earum illum veritatis commodi suscipit delectus sint repudiandae fugiat ad facilis mollitia. Minima nam eos nihil dolore eaque dicta explicabo, quidem perferendis quisquam provident consequuntur, placeat temporibus. Delectus reprehenderit cupiditate iste adipisci, sunt non, fugiat, culpa nam quis iusto ratione ipsum odio ab. Consequatur eum unde dolor id libero, minima ab aperiam dolores sit. Est quam, deleniti maiores veniam optio autem cum explicabo, atque consectetur minima dicta quis sed magni nobis, dolores. Possimus quidem maxime sequi exercitationem!', 'https://picsum.photos/200?random'),
(39, 'Новость 3', '2019-01-26', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos omnis molestias tenetur, tempora vero dignissimos explicabo quia placeat et doloremque quasi sequi adipisci earum illum veritatis commodi suscipit delectus sint repudiandae fugiat ad facilis mollitia. Minima nam eos nihil dolore eaque dicta explicabo, quidem perferendis quisquam provident consequuntur, placeat temporibus. Delectus reprehenderit cupiditate iste adipisci, sunt non, fugiat, culpa nam quis iusto ratione ipsum odio ab. Consequatur eum unde dolor id libero, minima ab aperiam dolores sit. Est quam, deleniti maiores veniam optio autem cum explicabo, atque consectetur minima dicta quis sed magni nobis, dolores. Possimus quidem maxime sequi exercitationem!', 'https://picsum.photos/200?random'),
(40, 'Новость 4', '2019-01-06', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos omnis molestias tenetur, tempora vero dignissimos explicabo quia placeat et doloremque quasi sequi adipisci earum illum veritatis commodi suscipit delectus sint repudiandae fugiat ad facilis mollitia. Minima nam eos nihil dolore eaque dicta explicabo, quidem perferendis quisquam provident consequuntur, placeat temporibus. Delectus reprehenderit cupiditate iste adipisci, sunt non, fugiat, culpa nam quis iusto ratione ipsum odio ab. Consequatur eum unde dolor id libero, minima ab aperiam dolores sit. Est quam, deleniti maiores veniam optio autem cum explicabo, atque consectetur minima dicta quis sed magni nobis, dolores. Possimus quidem maxime sequi exercitationem!', 'https://picsum.photos/200?random'),
(41, 'Новость 5', '2019-03-09', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos omnis molestias tenetur, tempora vero dignissimos explicabo quia placeat et doloremque quasi sequi adipisci earum illum veritatis commodi suscipit delectus sint repudiandae fugiat ad facilis mollitia. Minima nam eos nihil dolore eaque dicta explicabo, quidem perferendis quisquam provident consequuntur, placeat temporibus. Delectus reprehenderit cupiditate iste adipisci, sunt non, fugiat, culpa nam quis iusto ratione ipsum odio ab. Consequatur eum unde dolor id libero, minima ab aperiam dolores sit. Est quam, deleniti maiores veniam optio autem cum explicabo, atque consectetur minima dicta quis sed magni nobis, dolores. Possimus quidem maxime sequi exercitationem!', 'https://picsum.photos/200?random');

-- --------------------------------------------------------

--
-- Структура таблицы `objects`
--

CREATE TABLE `objects` (
  `objects_id` int(11) NOT NULL,
  `title` text NOT NULL,
  `customer` text NOT NULL,
  `object` text NOT NULL,
  `task` text NOT NULL,
  `performance_work` text NOT NULL,
  `important_date` text NOT NULL,
  `image_url` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `objects`
--

INSERT INTO `objects` (`objects_id`, `title`, `customer`, `object`, `task`, `performance_work`, `important_date`, `image_url`) VALUES
(37, 'Энергоснабжение Семеренковского ГКМ', 'ЗАО «Нафтогазвидобування»', 'СКТП 35/6кВ 2,5 МВт, Семеренковское газо-конденсатное месторождение, Полтавская область.', 'глубокий ввод для электроснабжения буровых установок.', '[\"\\u0440\\u0430\\u0437\\u0440\\u0430\\u0431\\u043e\\u0442\\u043a\\u0430 \\u043a\\u043e\\u043d\\u0446\\u0435\\u043f\\u0446\\u0438\\u0438;\\r\",\"\\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435;\\r\",\"\\u0441\\u043e\\u0433\\u043b\\u0430\\u0441\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435 \\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u0430;\\r\",\"\\u0441\\u0442\\u0440\\u043e\\u0438\\u0442\\u0435\\u043b\\u044c\\u043d\\u043e-\\u043c\\u043e\\u043d\\u0442\\u0430\\u0436\\u043d\\u044b\\u0435 \\u0440\\u0430\\u0431\\u043e\\u0442\\u044b \\u043f\\u043e \\u043f\\/\\u0441\\u0442;\\r\",\"\\u043f\\u0443\\u0441\\u043a\\u043e\\u043d\\u0430\\u043b\\u043e\\u0434\\u0447\\u043d\\u044b\\u0435 \\u0440\\u0430\\u0431\\u043e\\u0442\\u044b;\\r\",\"\\u0441\\u0442\\u0440\\u043e\\u0438\\u0442\\u0435\\u043b\\u044c\\u0441\\u0442\\u0432\\u043e \\u041b\\u042d\\u041f (\\u043d\\u0430 \\u043a\\u0430\\u0436\\u0434\\u0443\\u044e \\u0431\\u0443\\u0440\\u043e\\u0432\\u0443\\u044e \\u0443\\u0441\\u0442\\u0430\\u043d\\u043e\\u0432\\u043a\\u0443);\\r\",\"\\u043a\\u043e\\u043c\\u043f\\u043b\\u0435\\u043a\\u0441\\u043d\\u0430\\u044f \\u0441\\u0434\\u0430\\u0447\\u0430 \\u043e\\u0431\\u044a\\u0435\\u043a\\u0442\\u0430;\\r\",\"\\u043e\\u043f\\u0435\\u0440\\u0430\\u0442\\u0438\\u0432\\u043d\\u043e-\\u0442\\u0435\\u0445\\u043d\\u0438\\u0447\\u0435\\u0441\\u043a\\u043e\\u0435 \\u043e\\u0431\\u0441\\u043b\\u0443\\u0436\\u0438\\u0432\\u0430\\u043d\\u0438\\u0435.\"]', ' п/ст — 2006, новые ЛЭП — 2007, 2008, 2009, оперативно-техническое обслуживание — постоянно.', 'http://www.standartenergo.com.ua/uploads/2011/10/2011-10-25/sktp-35-6-klimovo.jpg#fullsize:/uploads/2011/10/2011-10-25/sktp-35-6-klimovo-0.jpg'),
(38, 'Энергоснабжение Покровского ГКМ', 'ООО «ПК «Газвидобування»', 'СКТП 35/6кВ 2,5 МВт, Покровское ГКР, Полтавская область.', 'глубокий ввод для электроснабжения буровых установок.', '[\"\\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435;\\r\",\"\\u0441\\u043e\\u0433\\u043b\\u0430\\u0441\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435 \\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u0430;\\r\",\"\\u0441\\u0442\\u0440\\u043e\\u0438\\u0442\\u0435\\u043b\\u044c\\u043d\\u043e-\\u043c\\u043e\\u043d\\u0442\\u0430\\u0436\\u043d\\u044b\\u0435 \\u0440\\u0430\\u0431\\u043e\\u0442\\u044b \\u043f\\u043e \\u043f\\/\\u0441\\u0442;\\r\",\"\\u043f\\u0443\\u0441\\u043a\\u043e\\u043d\\u0430\\u043b\\u043e\\u0434\\u0447\\u043d\\u044b\\u0435 \\u0440\\u0430\\u0431\\u043e\\u0442\\u044b;\\r\",\"\\u0441\\u0442\\u0440\\u043e\\u0438\\u0442\\u0435\\u043b\\u044c\\u0441\\u0442\\u0432\\u043e \\u0412\\u041b 6\\u043a\\u0412;\\r\",\"\\u043a\\u043e\\u043c\\u043f\\u043b\\u0435\\u043a\\u0441\\u043d\\u0430\\u044f \\u0441\\u0434\\u0430\\u0447\\u0430 \\u043e\\u0431\\u044a\\u0435\\u043a\\u0442\\u0430;\\r\",\"\\u043e\\u043f\\u0435\\u0440\\u0430\\u0442\\u0438\\u0432\\u043d\\u043e-\\u0442\\u0435\\u0445\\u043d\\u0438\\u0447\\u0435\\u0441\\u043a\\u043e\\u0435 \\u043e\\u0431\\u0441\\u043b\\u0443\\u0436\\u0438\\u0432\\u0430\\u043d\\u0438\\u0435.\"]', 'п/ст — 2007, оперативно-техническое обслуживание — постоянно.', 'http://www.standartenergo.com.ua/uploads/2011/10/2011-10-25/sktp-35-6-lavrency.jpg#fullsize:/uploads/2011/10/2011-10-25/sktp-35-6-lavrency-0.jpg'),
(39, 'Внешнее электроснабжение завода по производству биотоплива', 'ООО «УкрБиоТопливо»', 'СКТП 35/0,4кВ 1 МВт, г. Зеньков, Полтавская область', 'глубокий ввод для электроснабжения завода, вторая категория', '[\"\\u0440\\u0430\\u0437\\u0440\\u0430\\u0431\\u043e\\u0442\\u043a\\u0430 \\u043a\\u043e\\u043d\\u0446\\u0435\\u043f\\u0446\\u0438\\u0438;\\r\",\"\\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435;\\r\",\"\\u0441\\u043e\\u0433\\u043b\\u0430\\u0441\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435 \\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u0430;\\r\",\"\\u0441\\u0442\\u0440\\u043e\\u0438\\u0442\\u0435\\u043b\\u044c\\u043d\\u043e-\\u043c\\u043e\\u043d\\u0442\\u0430\\u0436\\u043d\\u044b\\u0435 \\u0440\\u0430\\u0431\\u043e\\u0442\\u044b \\u043f\\u043e \\u043f\\/\\u0441\\u0442;\\r\",\"\\u043f\\u0443\\u0441\\u043a\\u043e\\u043d\\u0430\\u043b\\u043e\\u0434\\u0447\\u043d\\u044b\\u0435 \\u0440\\u0430\\u0431\\u043e\\u0442\\u044b;\\r\",\"\\u0441\\u0442\\u0440\\u043e\\u0438\\u0442\\u0435\\u043b\\u044c\\u0441\\u0442\\u0432\\u043e \\u041b\\u042d\\u041f 35 \\u043a\\u0412;\\r\",\"\\u043a\\u043e\\u043c\\u043f\\u043b\\u0435\\u043a\\u0441\\u043d\\u0430\\u044f \\u0441\\u0434\\u0430\\u0447\\u0430 \\u043e\\u0431\\u044a\\u0435\\u043a\\u0442\\u0430;\\r\",\"\\u043e\\u043f\\u0435\\u0440\\u0430\\u0442\\u0438\\u0432\\u043d\\u043e-\\u0442\\u0435\\u0445\\u043d\\u0438\\u0447\\u0435\\u0441\\u043a\\u043e\\u0435 \\u043e\\u0431\\u0441\\u043b\\u0443\\u0436\\u0438\\u0432\\u0430\\u043d\\u0438\\u0435.\"]', 'начало проекта — март 2008, окончание — июль 2008.', 'http://www.standartenergo.com.ua/uploads/2011/10/2011-10-25/sktp-ukrbiotoplivo.jpg#fullsize:/uploads/2011/10/2011-10-25/sktp-ukrbiotoplivo-0.jpg'),
(40, 'Комплексный сервис энергохозяйства буровой установки №64 Семеренковского ГКМ', 'ЗАО «Нафтогазвидобування»', 'буровая установка №64, Семеренковское газо-конденсатное месторождение, Полтавская область', 'Комплексный сервис энергохозяйства буровой установки №64 Семеренковского ГКМ', '[\"\\u0440\\u0430\\u0437\\u0440\\u0430\\u0431\\u043e\\u0442\\u043a\\u0430 \\u043a\\u043e\\u043d\\u0446\\u0435\\u043f\\u0446\\u0438\\u0438;\\r\",\"\\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435;\\r\",\"\\u0441\\u043e\\u0433\\u043b\\u0430\\u0441\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435 \\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u0430;\\r\",\"\\u0430\\u0440\\u0435\\u043d\\u0434\\u0430 \\u0441\\u0438\\u043b\\u043e\\u0432\\u043e\\u0433\\u043e \\u0442\\u0440\\u0430\\u043d\\u0441\\u0444\\u043e\\u0440\\u043c\\u0430\\u0442\\u043e\\u0440\\u0430;\\r\",\"\\u0441\\u0442\\u0440\\u043e\\u0438\\u0442\\u0435\\u043b\\u044c\\u043d\\u043e-\\u043c\\u043e\\u043d\\u0442\\u0430\\u0436\\u043d\\u044b\\u0435 \\u0440\\u0430\\u0431\\u043e\\u0442\\u044b \\u043f\\u043e \\u043f\\/\\u0441\\u0442;\\r\",\"\\u043f\\u0443\\u0441\\u043a\\u043e\\u043d\\u0430\\u043b\\u043e\\u0434\\u0447\\u043d\\u044b\\u0435 \\u0440\\u0430\\u0431\\u043e\\u0442\\u044b;\\r\",\"\\u0441\\u0442\\u0440\\u043e\\u0438\\u0442\\u0435\\u043b\\u044c\\u0441\\u0442\\u0432\\u043e \\u041b\\u042d\\u041f 6 \\u043a\\u0412;\\r\",\"\\u044d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043c\\u043e\\u043d\\u0442\\u0430\\u0436\\u043d\\u044b\\u0435 \\u0440\\u0430\\u0431\\u043e\\u0442\\u044b 0,4 \\u043a\\u0412;\\r\",\"\\u043a\\u043e\\u043c\\u043f\\u043b\\u0435\\u043a\\u0441\\u043d\\u0430\\u044f \\u0441\\u0434\\u0430\\u0447\\u0430 \\u043e\\u0431\\u044a\\u0435\\u043a\\u0442\\u0430;\\r\",\"\\u043e\\u043f\\u0435\\u0440\\u0430\\u0442\\u0438\\u0432\\u043d\\u043e-\\u0442\\u0435\\u0445\\u043d\\u0438\\u0447\\u0435\\u0441\\u043a\\u043e\\u0435 \\u043e\\u0431\\u0441\\u043b\\u0443\\u0436\\u0438\\u0432\\u0430\\u043d\\u0438\\u0435\"]', 'ЛЭП — октябрь 2007, буровая установка — май 2008, оперативно-техническое обслуживание — постоянно.', 'http://www.standartenergo.com.ua/uploads/2011/10/2011-10-25/ktp.jpg#fullsize:/uploads/2011/10/2011-10-25/ktp-0.jpg');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`user_id`);

--
-- Индексы таблицы `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`clients_id`);

--
-- Индексы таблицы `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`news_id`);

--
-- Индексы таблицы `objects`
--
ALTER TABLE `objects`
  ADD PRIMARY KEY (`objects_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `admin`
--
ALTER TABLE `admin`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `clients`
--
ALTER TABLE `clients`
  MODIFY `clients_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT для таблицы `news`
--
ALTER TABLE `news`
  MODIFY `news_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT для таблицы `objects`
--
ALTER TABLE `objects`
  MODIFY `objects_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
