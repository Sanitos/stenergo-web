<footer>
		<a href="/">
			<div class="logo-footer">
					<img src="image/high-voltage-icon.png" alt="">
					<span><strong>СтандартЭнерго</strong><br>Высокое качество услуг</span>
			</div>
		</a>
		<div class="footer-menu">
			<ul class="footer-menu-list"> <span class="footer-menu-group"><a href="designing.php">Проектирование</a></span>
				<li><a href="designing.php?type=tp">ТП</a></li>
				<li><a href="designing.php?type=ktp">КТП</a></li>
				<li><a href="designing.php?type=lep">ЛЭП</a></li>
				<li><a href="designing.php?type=des">ДЭС</a></li>
				<li><a href="designing.php?type=luzod">ЛОСОД / АСКУЭ</a></li>
				<li><a href="designing.php?type=rza">РЗА</a></li>

			</ul>
			<ul class="footer-menu-list"> <span class="footer-menu-group"><a href="assembling.php">Монтаж</a></span>
				<li><a href="assembling.php?type=ktp">КТП</a></li>
				<li><a href="designing.php?type=lep">ЛЭП</a></li>
				<li><a href="designing.php?type=des">ДЭС</a></li>
				<li><a href="assembling.php?type=krnb">КРНБ</a></li>
			</ul>
			<ul class="footer-menu-list"> <span class="footer-menu-group"><a href="adjustment.php">Наладка</a></span>
				<li><a href="designing.php?type=rza">РЗА</a></li>
				<li><a href="adjustment.php?type=vru">ВРУ</a></li>
				<li><a href="adjustment.php?type=avr">Промышленные<br>системы <br>автоматики</a></li>
			</ul>
			<ul class="footer-menu-list"> <span class="footer-menu-group"><a href="sale.php">Продажа</a></span>
				<li><a href="sale.php#ktp">КТП</a></li>
				<li><a href="sale.php#tr-oil">Трансформаторное <br> масло</a></li>
				<li><a href="sale.php#silikagel">Силикагель</a></li>
				<li><a href="sale.php#electro">Электрозащитные <br> средства</a></li>
			</ul>
			<ul class="footer-menu-list"> <span class="footer-menu-group"><a href="electrolaboratory.php">Электролаборатория</a></span>
				<li><a href="electrolaboratory.php#measurements">Электроизмерительная лаборатория</a><br> </a></li>
				<li><a href="electrolaboratory.php#testing">Испытания <br> электрозащитных <br> средств</a></li>
			</ul>
		</div>
	</footer>
	
</body>
</html>


   <script type="text/javascript" src='js/jquery-3.3.1.min.js'></script>
  <script type="text/javascript" src="js/fs-gal.js"></script>
   
  <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

  <script src="js/script.js"></script>
<script type="text/javascript" src="js/admin.js"></script>

