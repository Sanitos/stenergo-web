<?php
	session_start();
 include_once("db_connecnt.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?php echo $page_title ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=0">

	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
	<link rel="stylesheet" type="text/css" href="slick/slick-theme.css">
	<link rel="icon" type="image/png" href="image/favicon.png" />
    <link rel="stylesheet" type="text/css" href="css/fs-gal.css" />
</head>
<body>
	<nav class="nav-bar">
		<div id="logo">
			<a href="/"><img src="image/opacity logo.png" alt=""></a>
		</div>
		<div id="menu">
			<ul >
				<li><a href="/">Главная страница</a></li>
				<li><a href="about_company.php">О компании</a></li>
				<li class = "sub-menu">
					<a href="services.php">Услуги</a>
					<div class="hide-menu">
						<ul>
							<li><a href="designing.php">Проектирование</a></li>
							<li><a href="assembling.php">Монтирование</a></li>
							<li><a href="adjustment.php">Наладка</a></li>
							<li><a href="sale.php">Продажа</a></li>
							<li><a href="electrolaboratory.php">Электролаборатория</a></li>
						</ul>
					</div>
				</li>
				<li><a href="clients.php">Наши клиенты</a></li>
				<li><a href="news.php">Новости компании</a></li>
				<li><a href="vacancy.php">Вакансии</a></li>
				<li><a href="contacts.php">Контакты</a></li>
				<?php if($_SESSION['is_auth']==true){?>
					<li><a href="admin.php">Админ</a></li>
					<?php
				} ?>
			</ul>
		</div>
	</nav>	
	<div class="banner">
		<span>Профессиональные услуги и оборудование <br>в промышленной энергетике</span>
	</div>	