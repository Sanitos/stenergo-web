<?php 
$page_title = "Главная";
include_once("header.php");
 ?>
<div class="wrap-content">

<div class="offers">
			<h2>Корпорация «СтандартЭнерго» предлагает</h2>
			<p class="p-style">Корпорация «СтандартЭнерго», предприятие с более чем двадцатилетним опытом работы на рынке электротехнических работ, предлагает своим заказчикам широкий спектр услуг по проектированию, строительству, монтажу, комплектации и техническому обслуживанию систем энергоснабжения. Наше предприятие обладает всем необходимым, для выполнения заказа любой степени сложности. На счету компании  <a href="assembling.php#objects">более 1200 успешных проектов</a>в в области энергообеспечения. Нашими заказчиками являются признанные лидеры в разных отраслях народного хозяйства Украины.</p>
			<div class="offers-slider">
				<div class='slide-wrap'><a href="assembling.php?type=ktp"><img src="image/promo-1.jpg" alt="promo1"><span class='slide-text'>Строительство подстанций "под ключ"</span></a></div>
				<div class='slide-wrap'><a href="designing.php"><img src="image/promo-2.jpg" alt="promo2"><span class='slide-text'>Проектирование внешнего электроснабжения</span></a></div>
				<div class='slide-wrap'><a href="designing.php?type=lep"><img src="image/promo-3.jpg" alt="promo3"><span class='slide-text'>Строительство ЛЭП</span></a></div>
				<div class='slide-wrap'><a href="electrolaboratory.php"><img src="image/promo-4.jpg" alt="promo4"><span class='slide-text'>Электролаборатория</span></a></div>
			</div>
			<p class="p-style">
				Гарантией успеха при выполнении Ваших заказов качественно и в установленные сроки, является наш многолетний опыт работы. Предприятие имеет в своем распоряжении все необходимое для выполнения работ оборудование и инструмент. Наш коллектив состоит из опытных и высококлассных проектировщиков, инженеров, менеджеров и рабочих. К каждому заказу и каждому заказчику мы подходим индивидуально, предлагая наиболее эффективные варианты решения поставленной задачи. Обратитесь к <a href="contacts.php">специалистам «СтандартЭнерго»</a> — это выгодно, эффективно и надежно!
			</p>
		</div>
		<div class="news">
			<h2>Новости компании</h2>
			<?php 
			$query='SELECT * FROM news ORDER BY date DESC LIMIT 3 ';
		    $stmn=$pdo->prepare($query);
		    $stmn->execute();
		    while($row=$stmn->fetch()){?>
				<div class="single-news" id="news<?php echo $row['news_id'] ?>">
					<span class="news-title"><a href="/news.php?id=<?php echo $row['news_id'] ?>"><?php echo $row['title'] ?></a></span>
					<sub><?php echo $row['date'] ?></sub>
					<p class="news-short"><?php 
					$output = explode(".", $row['text']);
									if(sizeof($output)>1){
										$output = array_slice($output, 0,1);
										echo join('. ', $output).".";
									}else{
										echo $row['text'];
									}
					 ?></p>
					<span class="news-full"><a href="/news.php?id=<?php echo $row['news_id'] ?>">Подробнее</a></span>
				</div>
		    <?php }
			 ?>
			
			
		</div>
	</div>
<?php 
include_once("footer.php")
 ?>
	
	
