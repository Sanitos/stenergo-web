$(".news_del").click(function() {
	const id=$(this).attr('id').slice(3);
	$.ajax({
		type:'post',
		url:'../php-script/admin-ajax.php', 
		data:({
			id:id,
			action:'delete',
			table:'news'
		})
	})
	$(this).parent().parent().remove();
});
$(".news_edt").click(function() {
	const id=$(this).attr('id').slice(3);
	$.ajax({
		type:'post',
		url:'../php-script/admin-ajax.php', 
		data:({
			id:id,
			action:'edit',
			table:'news'
		}),
		success : function(data){
			const json = JSON.parse(data);
			$('input[name=title]').val(json['title']);
			$('input[name=date]').val(json['date']);
			$('textarea').val(json['text']);
			$('input[name=image_url]').val(json['image_url']);
			$('input[name=id]').val(json['news_id']);
		}
	})
});
$(".client_del").click(function() {
	const id=$(this).attr('id').slice(3);
	$.ajax({
		type:'post',
		url:'../php-script/admin-ajax.php', 
		data:({
			id:id,
			action:'delete',
			table:'clients'
		})
	})
	$(this).parent().parent().remove();
});
$(".client_edt").click(function() {
	const id=$(this).attr('id').slice(3);
	$.ajax({
		type:'post',
		url:'../php-script/admin-ajax.php', 
		data:({
			id:id,
			action:'edit',
			table:'clients'
		}),
		success : function(data){
			const json = JSON.parse(data);
			$('input[name=name]').val(json['name']);
			$('select[name=industry]').val(json['industry']).prop('selected', true);
			$('input[name=image_url]').val(json['image_url']);
			$('input[name=id]').val(json['clients_id']);
			$('input[name=url]').val(json['url']);

		}
	})
});
$(".objects_del").click(function() {
	const id=$(this).attr('id').slice(3);
	$.ajax({
		type:'post',
		url:'../php-script/admin-ajax.php', 
		data:({
			id:id,
			action:'delete',
			table:'objects'
		})
	})
	$(this).parent().parent().remove();
});
$(".objects_edt").click(function() {
	const id=$(this).attr('id').slice(3);
	$.ajax({
		type:'post',
		url:'../php-script/admin-ajax.php', 
		data:({
			id:id,
			action:'edit',
			table:'objects'
		}),
		success : function(data){
			const json = JSON.parse(data);
			$('input[name=title]').val(json['title']);
			$('input[name=customer]').val(json['customer']);
			$('input[name=object]').val(json['object']);
			$('input[name=task]').val(json['task']);
			$('input[name=id]').val(json['objects_id']);
			const jsonString = JSON.parse(json['performance_work']);
			var perf1_work = "";
			for (var work in jsonString){
				perf1_work +=jsonString[work]+"\n";
			}
			var perf_work = perf1_work.slice(0, perf1_work.length-1);
			$('textarea').val(perf_work);
			$('input[name=important_date]').val(json['important_date']);
			$('input[name=image_url]').val(json['image_url']);

		}
	})
});