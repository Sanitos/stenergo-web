
$(document).ready(function(){
  $('.offers-slider').slick({
  	infinite: true,
  	slidesToShow: 2,
  	slidesToScroll: 1,
  	variableWidth: true,
  	autoplay: true,
  	dots: true,
  	speed: 600,
  	autoplaySpeed: 2500,
  	arrows: false
  });
});
