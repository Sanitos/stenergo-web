<?php 
	$page_title = 'Лицензии';
	include_once ("header.php");
?>
<div class="wrap-content flex-wrap">
          <h2>Лицензии и разрешительные документы</h2>
          <div class="license">
            <img src="image/license-1.jpg" alt="" class="fs-gal" data-url="image/license-1.jpg">
            <img src="image/license-2.jpg" alt="" class="fs-gal" data-url="image/license-2.jpg">
            <img src="image/license-3.jpg" alt="" class="fs-gal" data-url="image/license-3.jpg">
            <img src="image/license-4.jpg" alt="" class="fs-gal" data-url="image/license-4.jpg">
            <img src="image/license-5.jpg" alt="" class="fs-gal" data-url="image/license-5.jpg">
            <img src="image/license-6.jpg" alt="" class="fs-gal" data-url="image/license-6.jpg">
            <img src="image/license-7.jpg" alt="" class="fs-gal" data-url="image/license-7.jpg">
            <img src="image/license-8.jpg" alt="" class="fs-gal" data-url="image/license-8.jpg">
            <img src="image/license-9.jpg" alt="" class="fs-gal" data-url="image/license-9.jpg">
            <img src="image/license-10.jpg" alt="" class="fs-gal" data-url="image/license-10.jpg">
          </div>
          </div>
        <!-- Full screen gallery. -->
        <div class="fs-gal-view">
            <h1></h1>
            <img class="fs-gal-prev fs-gal-nav" src="image/prev.svg" alt="Previous picture" title="Previous picture" />
            <img class="fs-gal-next fs-gal-nav" src="image/next.svg" alt="Next picture" title="Next picture" />
            <img class="fs-gal-close" src="image/close.svg" alt="Close gallery" title="Close gallery" />
            <img class="fs-gal-main" src="" alt="" />
        </div>
<?php 
	include_once("footer.php");
?>