<?php 
session_start();
$page_title = "Новости";
include_once("header.php");
?>
<div class="wrap-content wrap-column">
<?php
if(isset($_GET['id'])){
	$news_id=$_GET['id'];
	$query='SELECT * FROM news WHERE news_id=?';
		$stmn=$pdo->prepare($query);
		$stmn->execute([$news_id]);
		while($row=$stmn->fetch()){?>
		<div class="single-news">
			<div >
				<span class='news-title no-center'><?php echo $row['title'] ?></span><span class="news-date"><?php echo $row['date'] ?></span>
			</div>
			<div class="center">
				<img src="<?php echo $row['image_url'] ?>">
				<p class="p-style"><?php 
					echo $row['text'];
 ?></p>
			</div>
			
		</div>
		<?php }
	 ?>

	<?php 
}else{?>
	
		<h2 class="news-h2">Новости компании</h2><br>
		<?php
		$query='SELECT * FROM news ';
		$stmn=$pdo->prepare($query);
		$stmn->execute();
		while($row=$stmn->fetch()){?>
		<div class="single-news">
			<div class="title-and-date">
				<span class='news-title'><a href="news.php?id=<?php echo $row['news_id'] ?>"><?php echo $row['title'] ?></a></span><span class="news-date"><?php echo $row['date'] ?></span>
			</div>
			<div class="image-and-text">
				<img src="<?php echo $row['image_url'] ?>">
				<p class="p-style"><?php 
				$output = explode(".", $row['text']);
				if(sizeof($output)>3){
					$output = array_slice($output, 0,2);
					echo join('. ', $output)."...";
				}else{
					echo $row['text'];
				}
				
 ?></p>
			</div>
			
		</div>
		<?php }
	 ?>

	
		<?php 
	}?>
	</div>
	<?php 
include_once("footer.php");

 ?>

