<?php
	$page_title = "Объекты";
	include_once("header.php");
?>
<div class="wrap-content ">

	<?php 
	$id =  $_GET['id'];
	$query = "SELECT * FROM objects WHERE objects_id=?";
	$stmn=$pdo->prepare($query);
	$stmn->execute([$id]);
	while($row=$stmn->fetch()){?>
		<div class="text object">
			<h2><?php echo $row['title'] ?></h2>
			<span><strong>Заказчик: </strong><?php echo $row['customer'] ?></span><br>
			<span><strong>Объект: </strong><?php echo $row['object'] ?></span><br>
			<span><strong>Задача: </strong><?php echo $row['task'] ?></span><br>
			<span><strong>Выполненные работы:</strong></span><br>
			<ul>
				<?php
					$performance_work = json_decode( $row['performance_work'], $assoc_array = false );
					foreach ($performance_work as $work) {?>
							<li><?php echo $work ?></li>
						<?php
					}
			 	?>
			</ul>
			<span><strong>Важные даты: </strong><?php echo $row['important_date'] ?></span><br>
		</div>
		<div class="img-rightof-text img-center">
			<img src="<?php echo $row['image_url'] ?>">
		</div>
	<?php }?>
</div>
<?php 
	include_once("footer.php");
?>