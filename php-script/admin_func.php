<?php
session_start();
	include_once("../db_connecnt.php");
if(isset($_POST['add_clients_btn'])){
	$section="add_clients";
	if(isset($_POST['name']) and isset($_POST['industry']) and isset($_POST['image_url']) and isset($_POST['url'])){
		$name = $_POST['name'];
		$industry = $_POST['industry'];
		// $image_url = $_POST['image_url'];
		if(!empty($_FILES['userfile']['name'])){
			$uploaddir = 'C:/OSPanel/domains/standartenergo/image/';
			$uploadfile = $uploaddir.basename($_FILES['userfile']['name']);
			$uploadfile_type = $_FILES['userfile']['type'];
			$allow_type = array("image/jpeg", "image/png");
			if(in_array($uploadfile_type, $allow_type)){
				if(move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)){
					$image_url= "image/".basename($_FILES['userfile']['name']);
				}
				else{
					echo " not saved";
				}
			}else{
				echo "недопустимый файл, попробуйте еще раз";
				print_r($_FILES);
			}
		}else{
			$image_url = $_POST['image_url'];
		}
		$url = $_POST['url'];
		if($_POST['id']!==""){
			$result = "UPDATE clients SET name=?,industry=?,image_url=?, url=? WHERE clients_id = ?";
			$stmn=$pdo->prepare($result);
			$stmn->execute([$name,$industry,$image_url,$url, $_POST['id']]);
		}else{
			$result="INSERT INTO clients (name, industry, image_url, url) VALUES (?,?,?,?)";
			$stmn=$pdo->prepare($result);
			$stmn->execute([$name,$industry,$image_url,$url]);
		}
		

	}
}
if(isset($_POST['add_news_btn'])){
	$section="add_news";
	if(isset($_POST['title']) and isset($_POST['date']) and isset($_POST['text'])){
		$title = $_POST['title'];
		$date = $_POST['date'];
		$text = $_POST['text'];
		if(!empty($_FILES['userfile']['name'])){
			$uploaddir = 'C:/OSPanel/domains/standartenergo/image/';
			$uploadfile = $uploaddir.basename($_FILES['userfile']['name']);
			$uploadfile_type = $_FILES['userfile']['type'];
			$allow_type = array("image/jpeg", "image/png");
			if(in_array($uploadfile_type, $allow_type)){
				if(move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)){
					$image_url= "image/".basename($_FILES['userfile']['name']);
				}
				else{
					echo " not saved";
				}
			}else{
				echo "недопустимый файл, попробуйте еще раз";
				print_r($_FILES);
			}
		}else{
			$image_url = $_POST['image_url'];
		}
		if($_POST['id']!==""){
			$result = "UPDATE news SET title=?, date=?, text=?, image_url=? WHERE news_id=?";
			$stmn=$pdo->prepare($result);
			$stmn->execute([$title,$date,$text,$image_url, $_POST['id']]);
		}else{
			$result="INSERT INTO news (title, date, text, image_url) VALUES (?,?,?,?)";
			$stmn=$pdo->prepare($result);
			$stmn->execute([$title,$date,$text,$image_url]);
		}
		

	}
}
if(isset($_POST['add_object_btn'])){
	$section="add_object";
	$title = $_POST['title'];
	$customer = $_POST['customer'];
	$object = $_POST['object'];
	$task = $_POST['task'];
	$performance_work_array = explode("\n",trim($_POST['performance_work']));
	$performance_work = json_encode($performance_work_array);
	$important_date = $_POST['important_date'];
	if(!empty($_FILES['userfile']['name'])){
			$uploaddir = 'C:/OSPanel/domains/standartenergo/image/';
			$uploadfile = $uploaddir.basename($_FILES['userfile']['name']);
			$uploadfile_type = $_FILES['userfile']['type'];
			$allow_type = array("image/jpeg", "image/png");
			if(in_array($uploadfile_type, $allow_type)){
				if(move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)){
					$image_url= "image/".basename($_FILES['userfile']['name']);
				}
				else{
					echo " not saved";
				}
			}else{
				echo "недопустимый файл, попробуйте еще раз";
				print_r($_FILES);
			}
		}else{
			$image_url = $_POST['image_url'];
		}
	if($_POST['id']!==""){
		$result="UPDATE objects SET title=?, customer=?, object=?, task=?,performance_work=?,important_date=?,image_url=? WHERE objects_id=? ";
		$stmn=$pdo->prepare($result);
		$stmn->execute([$title,$customer,$object,$task,$performance_work,$important_date, $image_url, $_POST['id']]);

	}else{
		$result="INSERT INTO objects (title, customer, object, task,performance_work,important_date,image_url) VALUES (?,?,?,?,?,?,?)";
		$stmn=$pdo->prepare($result);
		$stmn->execute([$title,$customer,$object,$task,$performance_work,$important_date, $image_url]);
		$_POST = array();

	}	
	
}
header("Location:/admin_panel.php?section=".$section)
?>