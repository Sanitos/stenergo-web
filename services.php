<?php
	$page_title = "Услуги";
	include_once ("header.php");
?>
<div class="wrap-content">
	<div class="text">
		<h2>Предоставляемые услуги</h2>
		<ul>
			<li><a href="designing.php">Проектирование</a></li>
			<li><a href="assembling.php">Монтирование</a></li>
			<li><a href="adjustment.php">Наладка</a></li>
			<li><a href="sale.php">Продажа</a></li>
			<li><a href="electrolaboratory.php">Электролаборатория</a></li>
		</ul>
	</div>
</div>
<?php 
	include_once ("footer.php");
?>