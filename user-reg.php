<?php 
	session_start();
	include_once ("db_connecnt.php");
	function generateSalt(){//генерация соли
		$salt='';
		for($i=0;$i<8;$i++){
			$salt .=chr(mt_rand(33,126));
		}
		return $salt;
	}
	if(isset($_POST['user_reg_btn'])){
		$login=$_SESSION['login'];

		$new_login=$_POST['login'];
		$salt = generateSalt();
		$password=md5($_POST['password'].$salt);
		$query = "UPDATE admin SET login=?, salt=?, password=? WHERE login=?";
		$stmn=$pdo->prepare($query);
		$stmn->execute([$new_login, $salt, $password,$login]);
		header("Location:admin.php");
		exit;

	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Регистрация администратора</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">

</head>
<body>
	<div class="login-form-wrap" >
			<form class="login-form" action="" method="post" >
				<a href="/"><img src="image/opacity logo.png"></a>
				<span class='whatis'>Введите новые данные администратора</span><br>

				<span>Имя пользователя</span>
				<input  type="text" name="login" placeholder="Имя пользователя">
				<span>Пароль</span>
				<input autocomplete="new-password" type="password" name="password" placeholder="Пароль">
				<input type="submit" name="user_reg_btn" value="Войти">
			</form>
		</div>
</body>
</html>