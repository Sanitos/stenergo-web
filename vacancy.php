<?php 
	$page_title = "Вакансии";
	include_once ('header.php');
?>
<div class="wrap-content wrap-column">
	<p class="p-style">
		<strong>
			ООО "СтандартЭнерго" всегда открыта к новым сотрудникам! Мы предлагаем вам работу в молодой дружной команде, направленной на эффективный труд и качественный результат. Если вы инициативны, дружественны и коммуникабельны - ждём вас!
		</strong>
		<div class="vacancy">
			<span class="title">Открыта вакансия системного администратора!</span>
			<p class="requirements">Требования к соискателю: мужчина, возраст - 19-40 лет, высшее или неполное высшее образование. Ответственность, высокая обучаемость.</p>
			<p class="responsibility">
				<strong>Основные обязанности:</strong>
				<ul>
					<li>Lorem ipsum dolor sit amet, consectetur adipisicing.</li>
					<li>Lorem ipsum dolor sit amet, consectetur adipisicing.</li>
					<li>Lorem ipsum dolor sit amet, consectetur adipisicing.</li>
					<li>Lorem ipsum dolor sit amet, consectetur adipisicing.</li>
					<li>Lorem ipsum dolor sit amet, consectetur adipisicing.</li>
					<li>Lorem ipsum dolor sit amet, consectetur adipisicing.</li>
					<li>Lorem ipsum dolor sit amet, consectetur adipisicing.</li>
				</ul>
			</p>
			<p class="vacancy-offer">
				<strong>Мы предлагаем:</strong>
				<ul>
					<li>Lorem ipsum dolor sit amet.</li>
					<li>Lorem ipsum dolor sit amet.</li>
					<li>Lorem ipsum dolor sit amet.</li>
					<li>Lorem ipsum dolor sit amet.</li>
					<li>Lorem ipsum dolor sit amet.</li>
				</ul>
			</p>
			<p class="procedure">
				<strong>Процедура конкурса: </strong>Для получения анкеты кандидата обратитесь по адресу: пгт Опошня, ул. Заливчего, 71 или сообщите об интересе к нашей вакансии по тел. 42-956, 42-987, 43-467, оставив свой домашний адрес: мы вышлем Вам анкету по почте. Чтобы получить анкету в электронной форме, сделайте запрос: Office@standartenergo.com.
			</p>
			<p class="additional-text">
				<strong>Внимание! </strong>Мы не принимаем на работу по знакомству. <br>Гарантируем конфиденциальность Вашего обращения.
			</p>
		</div>
	</p>
</div>
<?php 
	include_once ("footer.php");
?>